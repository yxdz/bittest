﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.App_Fifter
{
    public class AccountFilterAttribute: ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext) {

            if (filterContext.HttpContext.Session["user_name"] == null)
            {   string WebSite = ConfigurationManager.AppSettings["WebSite"];
                filterContext.Result = new RedirectResult(WebSite);

            }


        }
    }
}