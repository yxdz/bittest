﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections;

namespace WebApplication1.App_DLL
{
    public class DataBase
    {
        public SqlConnection myConn = null;
        public SqlCommand comm = null;
        public SqlTransaction tx = null;
        private DataBase instance = null;
        //private static DataBase instance = null;

        string conStr = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
        public DataBase()
        {
            try
            {
                if (instance == null)
                {
                    myConn = new SqlConnection(conStr);
                    myConn.Open();
                    comm = new SqlCommand();
                    comm.Connection = myConn;
                    instance = this;
                }
                else
                {
                    myConn = instance.myConn;
                    comm = instance.comm;
                    comm.Cancel();                  
                    if (myConn == null)
                    {
                        myConn.Open();
                    }
                    else if (myConn.State == System.Data.ConnectionState.Closed)
                    {
                        myConn.Open();
                    }
                    else if (myConn.State == System.Data.ConnectionState.Broken)
                    {                      
                        myConn.Open();
                    }

                }
            }
            catch
            {
                myConn.Close();
                myConn.Dispose();
                myConn = new SqlConnection(conStr);
                myConn.Open();
                comm = new SqlCommand();
                comm.Connection = myConn;
                instance = this;

            }
        }
        //关闭数据库对象
        public void Close()
        {
            myConn.Close();
            myConn.Dispose();
        }
        public void Transaction()
        {

            tx = myConn.BeginTransaction(IsolationLevel.ReadCommitted);//初始化事务
            comm.Transaction = tx;//绑定事务
        }
        public void Commit()
        {
            tx.Commit();

        }
        public void Rollback()
        {

            tx.Rollback();
        }
        public DataTable GetRecords(string SqlText)
        {
            comm.CommandText = SqlText;
            SqlDataAdapter da = new SqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;

        }//查询返回table
        public string getLastRecords(string SqlText, string item)
        {
            comm.CommandText = SqlText;
            SqlDataAdapter da = new SqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt.Rows.Count != 0)
            {
                return (dt.Rows[dt.Rows.Count - 1][item].ToString());
            }
            else return "null";
        }//返回最后一个数据
        public string InsertInfo(string sqltext)
        {
            try
            {
                comm.CommandText = sqltext;
                comm.ExecuteNonQuery();

                return "true";
            }
            catch (Exception e)
            {
                return e.Message;

            }


        }//插入数据，删除
        public void InsertInfo2(string sqltext)
        {

            comm.CommandText = sqltext;
            comm.ExecuteNonQuery();
        }
        //public SqlDataReader selectInfo(string sqltext){
        //    comm.CommandText = sqltext;
        //    SqlDataReader dr = comm.ExecuteReader();

        //    return dr;
        //}//查询返回结果集
        public bool checkstaff(string staffid, string password)
        {
            comm.CommandText = "select * from staffInfo where staffid=" + staffid + " and password=" + password + "";
            SqlDataAdapter da = new SqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            // DataTable dt2 = new DataTable();
            bool result;
            if (dt.Rows.Count != 0)
            {
                result = true;
            }
            else
            {

                result = false;
            }

            return result;
        } //验证用户名密码

        //20150317
        public string loginstate(string staffid, string mac)
        {

            comm.CommandText = "select prefsession from StaffInfo where staffid='" + staffid + "' and prefsession='" + mac + "'";
            SqlDataAdapter da = new SqlDataAdapter(comm);

            DataTable dt = new DataTable();
            da.Fill(dt);

            string result = "false";
            if (dt.Rows.Count == 0)
            {
                result = "0";

            }
            else
            {
                result = "1";

            }

            return result;

        }
        public DataTable Select(string cmd, SqlParameter[] parameters)
        {
            comm.Parameters.Clear();
            comm.CommandText = cmd;
            comm.Parameters.AddRange(parameters);
            SqlDataAdapter da = new SqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            comm.Parameters.Clear();
            return dt;
        }
        public int UMI(string cmd, SqlParameter[] parameters)
        {
            comm.Parameters.Clear();
            comm.CommandText = cmd;
            comm.Parameters.AddRange(parameters);
            return comm.ExecuteNonQuery();
        }
    }
}