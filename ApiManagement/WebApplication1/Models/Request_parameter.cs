﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Request_parameter
    {
        List<Request_parameter_item> request_parameter_item { get; set; }
    }
    public class Request_parameter_item {



        public int parameter_id { get; set; }
        public int sid { get; set; }
        public string parameter_name { get; set; }
        public string parameter_type { get; set; }
        public int is_required { get; set; }
        public string parameter_description { get; set; }

    }
}