﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebApplication1.App_DLL;
using WebApplication1.App_Fifter;
using WebApplication1.App_Handler;

namespace WebApplication1.Controllers.ApiManage
{
    [AccountFilter]
    public class ApiManagementController : Controller
    {
        // GET: ApiManagement
        public ActionResult Index(string id,string sid,int version)
        {
            ViewBag.sid = sid;
            ViewBag.id = id;
            ViewBag.version = version;
            ViewBag.user_name = Session["user_name"];
            return View();
        }
        public ActionResult History(string sid) {
            ViewBag.user_name = Session["user_name"];
            ViewBag.sid = sid;
            return View();
        }
        public string GetHistory(string sid) {         
            DataTable dt3 = new DataTable();
            DataBase db = new DataBase();
            SqlParameter[] parameters = new SqlParameter[] { new System.Data.SqlClient.SqlParameter("@sid", sid)};
            string cmdtex3 = "select id,sid,api_name,version,create_time,change_reason,user_name from Api_content where sid=@sid";//获取旧版本API列表
            
            dt3 = db.Select(cmdtex3,parameters);
            db.Close();
            return JSONHandler.Dtb2Json(dt3);
        }
        public ActionResult Add() {

            ViewBag.user_name = Session["user_name"];
            return View();
        }
        public string GetApiList() {
            DataTable dt = new DataTable();
            DataBase db = new DataBase();
            string cmdtex = "select b.id,b.sid,b.api_name,b.api_description,b.version  from (select sid,max(version) version from Api_content group by sid) as a,Api_content as b where a.sid=b.sid and a.version=b.version order by b.api_name";
            dt = db.GetRecords(cmdtex);
            db.Close();
            if (dt.Rows.Count > 0)
            {
                return JSONHandler.Dtb2Json(dt);
            }
            else {
                return null;
            }
        }
        public string GetApiList_Ajax(string apiname) {
            DataTable dt = new DataTable();
            DataBase db = new DataBase();
            string cmdtex = "select top 10 b.id,b.sid,b.api_name,b.api_description,b.version  from (select sid,max(version) version from Api_content group by sid) as a,Api_content as b where a.sid=b.sid and a.version=b.version  and b.api_name like '%"+apiname+"%'  order by b.api_name";
            dt = db.GetRecords(cmdtex);
            db.Close();
            if (dt.Rows.Count > 0)
            {
                return JSONHandler.Dtb2Json(dt);
            }
            else
            {
                return null;
            }
        }
        public string GetApiContent(string id,string sid) {
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();             
            DataBase db = new DataBase();
            string result = "";
            SqlParameter[] parameters = new SqlParameter[] { new System.Data.SqlClient.SqlParameter("@id", id) };
            string cmdtex1 = "select * from Api_content where id=@id";//获取该版本（文档）的内容
            string cmdtex2 = "select * from Api_parameter where id=@id"; //获取该版本（文档）的参数内容                
            dt1 = db.Select(cmdtex1,parameters);
            dt2 = db.Select(cmdtex2,parameters);      
            result = "{\"main\":" + JSONHandler.Dtb2Json(dt1) + ",\"parameter\":" + JSONHandler.Dtb2Json(dt2) + "}";
            db.Close();
            return result;

        }
        public string AddContent()
        {
            if (Session["user_name"] != null)
            {
                string user_name = Session["user_name"].ToString();
                string result = "";
                JavaScriptSerializer jss = new JavaScriptSerializer();
                if (Request.Form["api_name"] != null)
                {
                    Guid id = Guid.NewGuid();
                    //判断是修改还是新增
                    long sid;
                    int version;
                    var change_reason="";
                    if (Request.Form["sid"] != null && Request.Form["version"] != null && Request.Form["sid"] != "" && Request.Form["version"] != "")
                    {
                        sid = Convert.ToInt64(Request.Form["sid"]);
                        version = Convert.ToInt32(Request.Form["version"]) + 1;
                       
                    }
                    else
                    {
                        
                        sid = TimerHandler.GetTimeStamp();
                        version = 0;
                    }
                    if (Request.Form["change_reason"] != null && Request.Form["change_reason"] != "")
                    {
                        change_reason = Request.Form["change_reason"];
                    }
                    else {
                        change_reason = "";
                    }
                    var api_name = Request.Form["api_name"];
                    var api_description = Request.Form["api_description"];
                    var api_introduction = Request.Form["api_introduction"];
                    var request_url = Request.Form["request_url"];
                    var request_method = Request.Form["request_method"];
                    var return_value_type = Request.Form["return_value_type"];
                    var request_sample = Request.Form["request_sample"];
                    var response_sample = Request.Form["response_sample"];
                    var unnormal_response_sample = Request.Form["unnormal_response_sample"];
                    var error_code_explanation = Request.Form["error_code_explanation"];
                    var remark = Request.Form["remark"];
                    var create_time = DateTime.Now;
                    var online_status = 3;//0计划中、1开发中、2已上线、3待下线、4已下线
                    var is_history = 0;//0不是历史记录、1是历史记录
                    var check_status = 0;//0待核验、1核验已通过、2核验未通过
                    var is_deleted = 0;//0未删除、1已删除
                    var request_parameter = jss.Deserialize(Request.Form["request_parameter"], typeof(string[][])) as string[][];
                    // List<Request_parameter> request_parameter = jss.Deserialize<List<Request_parameter>>(Request.Form["request_parameter"]);
                    var response_parameter = jss.Deserialize(Request.Form["response_parameter"], typeof(string[][])) as string[][];
                   
                    DataBase db = new DataBase();
                    db.Transaction();
                    try
                    {
                        SqlParameter[] parameters = new SqlParameter[] {
                        new SqlParameter("@id", id),
                        new SqlParameter("@sid", sid),
                        new SqlParameter("@api_name", api_name),
                        new SqlParameter("@api_description", api_description),
                        new SqlParameter("@api_introduction", api_introduction),
                        new SqlParameter("@request_url", request_url),
                        new SqlParameter("@request_method", request_method),
                        new SqlParameter("@return_value_type", return_value_type),
                        new SqlParameter("@request_sample", request_sample),
                        new SqlParameter("@response_sample", response_sample),
                        new SqlParameter("@unnormal_response_sample", unnormal_response_sample),
                        new SqlParameter("@error_code_explanation", error_code_explanation),
                        new SqlParameter("@remark", remark),
                        new SqlParameter("@version", version),
                        new SqlParameter("@create_time", create_time),
                        new SqlParameter("@online_status", online_status),
                        new SqlParameter("@is_history", is_history),
                        new SqlParameter("@check_status", check_status),
                        new SqlParameter("@is_deleted", is_deleted),
                        new SqlParameter("@change_reason", change_reason),
                        new SqlParameter("@user_name", user_name),
                        };
                        string cmdtex = "insert into Api_content (id,sid,api_name,api_description,api_introduction,request_url,request_method,return_value_type,request_sample,response_sample,unnormal_response_sample,error_code_explanation,remark,version,create_time,online_status,is_history,check_status,is_deleted,change_reason,user_name)";
                        cmdtex += " values(@id,@sid,@api_name,@api_description,@api_introduction,@request_url,@request_method,@return_value_type,@request_sample,@response_sample,@unnormal_response_sample,@error_code_explanation,@remark,@version,@create_time,@online_status,@is_history ,@check_status ,@is_deleted ,@change_reason,@user_name)";
                        string cmdtex2 = "";
                        if (request_parameter.Length > 0)
                        {
                            for (int i = 0; i <= request_parameter.Length - 1; i++)
                            {
                                Guid parameter_id = Guid.NewGuid();
                                SqlParameter[] parameters2 = new SqlParameter[] {
                                  new SqlParameter("@parameter_id", parameter_id),
                                  new SqlParameter("@id", id),
                                  new SqlParameter("@parameter_name", request_parameter[i][0]),
                                  new SqlParameter("@parameter_type", request_parameter[i][1]),
                                  new SqlParameter("@parameter_length", request_parameter[i][2]),
                                  new SqlParameter("@is_required", request_parameter[i][5]),
                                  new SqlParameter("@parameter_description", request_parameter[i][3]),
                                  new SqlParameter("@parameter_example", request_parameter[i][4]),  
                                };
                               
                                cmdtex2 = "insert into Api_parameter (parameter_id,id,parameter_name,parameter_type,parameter_length,is_required,parameter_description,parameter_example,parameter_rank,parameter_from) values(@parameter_id,@id,@parameter_name,@parameter_type,@parameter_length,@is_required,@parameter_description,@parameter_example," + i + ",1);";
                                db.UMI(cmdtex2, parameters2);
                            }
                        }
                        if (response_parameter.Length > 0)
                        {
                            for (int j = 0; j <= response_parameter.Length - 1; j++)
                            {
                                Guid parameter_id = Guid.NewGuid();
                                SqlParameter[] parameters2 = new SqlParameter[] {
                                  new SqlParameter("@parameter_id", parameter_id),
                                  new SqlParameter("@id", id),
                                  new SqlParameter("@parameter_name", response_parameter[j][0]),
                                  new SqlParameter("@parameter_type", response_parameter[j][1]),
                                  new SqlParameter("@parameter_length", response_parameter[j][2]),
                                  new SqlParameter("@is_required", response_parameter[j][5]),
                                  new SqlParameter("@parameter_description", response_parameter[j][3]),
                                  new SqlParameter("@parameter_example", response_parameter[j][4]),
                                };
                                cmdtex2 = "insert into Api_parameter (parameter_id,id,parameter_name,parameter_type,parameter_length,is_required,parameter_description,parameter_example,parameter_rank,parameter_from) values(@parameter_id,@id,@parameter_name,@parameter_type,@parameter_length,@is_required,@parameter_description,@parameter_example," + j + ",0);";
                                db.UMI(cmdtex2, parameters2);
                            }


                        }
                        db.UMI(cmdtex,parameters);//主体内容
                        //db.InsertInfo2(cmdtex2);//参数内容
                        db.Commit();
                        db.Close();
                        return "{\"id\":\""+id+ "\",\"sid\":\"" + sid + "\",\"version\":\"" + version+"\",\"result\":\"ture\"}";
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        db.Close();
                        return e.Message;
                    }
                }

                else
                {
                    return "{\"result\":\"no\"}";
                }

            }
            else
            {

                return "{\"result\":\"logout\"}";
            } 
        }
       
    }
}