﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.App_DLL;

namespace WebApplication1.Controllers.ApiManage
{
    public class ApiAccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return PartialView();
        }
        public string register() {
             DataBase db = new DataBase();
            try
            {
                string api_name = Request.Form["api_name"];
                string api_password = Request.Form["api_password"];
                string api_email = Request.Form["api_email"];
                string api_photo = Request.Form["api_photo"];
                Guid id = Guid.NewGuid();
                DateTime create_time = DateTime.Now;
                SqlParameter[] parameters = new SqlParameter[] {
                    new SqlParameter("@id", id),
                     new SqlParameter("@api_name", api_name),
                    new SqlParameter("@api_password", api_password),  
                    new SqlParameter("@api_photo", api_photo),
                    new SqlParameter("@api_email", api_email),
                     new SqlParameter("@create_time", create_time),
                };
                string cmdtex = "insert into Api_User (id,user_name,password,mobile,email,create_time) values(@id,@api_name,@api_password,@api_photo,@api_email,@create_time)";
                db.Select(cmdtex,parameters);
                db.Close();
                Session["user_name"] = api_name;
                return "true";
            }
            catch(Exception e){
                db.Close();
                return e.Message;
            }
        }

        public ActionResult Login()
        {
            return PartialView();
        }
        public string check() {
            string user_name = Request.Form["api_name"];
            string password = Request.Form["api_password"];
            DataBase db = new DataBase();
            DataTable dt = new DataTable();
            SqlParameter[] parameters = new SqlParameter[] {
                        new SqlParameter("@user_name", user_name),new SqlParameter("@password", password) };
            string cmdtex = "select id from Api_User where user_name=@user_name and password=@password";
            dt = db.Select(cmdtex,parameters);
            db.Close();
            if (dt.Rows.Count > 0)
            {
                Session["user_name"] = user_name;
                return "true";
            }
            else {
                return "false";

            }
        }

        public ActionResult logout() {

            Session.Abandon();

            return RedirectToAction("index", "Home");

        }
    }
}